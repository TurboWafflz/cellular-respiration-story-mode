﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define w = Character("Whoknoos")
define c = Character("CEO")
define cr = Character("CeloRes Representative")
define v = Character("Mr. Evil")

default fermentation = False
default stolen = False
default celoRes = False
default dreams = 0


label start:
    $ dreams = 0
    python:
        if not persistent.set_volumes:
            persistent.set_volumes = True
            _preferences.volumes['music'] *= .35
    play music "audio/voxel-revolution.ogg"

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene board_room

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.

    show ceo_worried

    # These display lines of dialogue.
    voice "audio/voice/ceo_demand.ogg"
    c "MitoConn productions are unable to keep up with demand.\n\n(Click or press space to advance dialog)"
    voice "audio/voice/ceo_efficient.ogg"
    c "We need a more efficient way to store energy in chemical bonds so we can transfer it to our customers."

    hide ceo_worried
    show whoknoos_happy

    voice "audio/voice/whoknoos_solution.ogg"
    w "I think I may have a solution,"

    jump question1

label question1:
    scene board_room

    show whoknoos_happy
    voice "audio/voice/whoknoos_respiration.ogg"
    w "All we need is cellular respiration."

    hide whoknoos_happy
    show ceo_hopeful
    voice "audio/voice/ceo_respiration.ogg"
    c "What is cellular respiration?"

    hide ceo_hopeful
    show whoknoos_happy
    menu:
        "What is cellular respiration?"

        "Cellular respiration is a process that converts glucose into water.":
            voice "audio/voice/whoknoos_glucose2water.ogg"
            w "Cellular respiration is a process that converts glucose into water."
            jump question1Inconrrect

        "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ATP.":
            voice "audio/voice/whoknoos_atp.ogg"
            w "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ATP."
            jump question1Correct

        "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ADP.":
            voice "audio/voice/whoknoos_adp.ogg"
            w "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ADP."
            jump question1Inconrrect

label question1Inconrrect:
    scene board_room
    show ceo_disappointed

    voice "audio/voice/ceo_incorrect.ogg"
    c "That can't possibly be correct!"

    hide ceo_disappointed
    show whoknoos_happy

    voice "audio/voice/whoknoos_misspoke.ogg"
    w "I'm sorry, I misspoke."

    menu:
        "What is cellular respiration?"

        "Cellular respiration is a process that converts glucose into water.":
            voice "audio/voice/whoknoos_glucose2water.ogg"
            w "Cellular respiration is a process that converts glucose into water."
            jump question1Inconrrect

        "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ATP.":
            voice "audio/voice/whoknoos_atp.ogg"
            w "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ATP."
            jump question1Correct

        "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ADP.":
            voice "audio/voice/whoknoos_adp.ogg"
            w "Cellular respiration converts oxygen and glucose to water, carbon dioxide, and energy in the form of ADP."
            jump question1Inconrrect

label question1Correct:
    scene board_room
    show ceo_hopeful

    voice "audio/voice/ceo_solution.ogg"
    c "That might just be the solution!"

    jump endOfMeeting


label endOfMeeting:
    scene board_room
    "And so Whoknoos began preparing to install a cellular respiration system in the MitoConn facility."
    jump phoneCall

label phoneCall:
    if dreams >= 10:
        stop music
        image exception_bg = "#dadada"
        image fake_exception = Text("An exception has occurred.", size=40, style="_default")
        image fake_exception2 = Text("That's quite enough of that.", size=20, style="_default")
        show exception_bg zorder 2
        show fake_exception zorder 2:
            xpos 0.1 ypos 0.05
        show fake_exception2 zorder 2:
            xpos 0.1 ypos 0.15
        $ renpy.pause()
        return
    scene office
    show cellores_talking

    voice "audio/voice/cellores_greeting.ogg"
    cr "Thank you for calling, this is Jason, how may I help you?"

    hide cellores_talking
    show whoknoos_happy

    voice "audio/voice/whoknoos_greeting.ogg"
    w "Hi, I'm Whooknoos from MitoConn. I would like to purchase one of your cellular respiration systems for my company."

    hide whoknoos_happy
    show cellores_talking

    voice "audio/voice/cellores_requirements.ogg"
    cr "Do you have any specific requirements?"

    hide cellores_talking
    show whoknoos_happy

    menu:
        "Do you have any specific requirements?"

        "Yes, I would like a system that supports fermentation.":
            voice "audio/voice/whoknoos_requirement.ogg"
            w "Yes, I would like a system that supports fermentation."
            $ fermentation = True
        "No, just give me whatever the cheapest is":
            $ fermentation = False
            voice "audio/voice/whoknoos_cheapest.ogg"
            w "No, just give me whatever the cheapest is"

    hide whoknoos_happy
    show cellores_talking

    if fermentation:
        voice "audio/voice/cellores_cr750.ogg"
        cr "I recommend the CR-750, it supports fermentation and is available for only $49,999."
    else:
        voice "audio/voice/cellores_cr253.ogg"
        cr "Our budget offering is the CR-253, it doesn't support fermentation, but is competitively priced at only $32,599."

    hide cellores_talking
    show whoknoos_happy

    voice "audio/voice/whoknoos_ok.ogg"
    w "I'll go with that then. You probably know a lot more about this than me."

    hide whoknoos_happy
    show cellores_talking

    if fermentation:
        voice "audio/voice/cellores_49.ogg"
        cr "Ok, that will be $49,999, how will you be paying?"
    else:
        voice "audio/voice/cellores_32.ogg"
        cr "That will be $32,599. How would you like to pay?"

    hide cellores_talking
    show whoknoos_happy

    menu:
        "How would you like to pay?"

        "The company credit card, it's a MasterCard":
            voice "audio/voice/whoknoos_mastercard.ogg"
            w "The company credit card, it's a MasterCard"
            pass
        "Check":
            voice "audio/voice/whoknoos_check.ogg"
            w "Check"
            pass
        "Not at all, this is a robbery!":
            stop music
            play music "audio/son-of-a-rocket.ogg"

            voice "audio/voice/whoknoos_robbery.ogg"
            w "Not at all, this is a robbery!"

            jump robbery

    hide whoknoos_happy
    show cellores_talking

    voice "audio/voice/cellores_thanks.ogg"
    cr "Thank you, you should recieve your system in the next 5-7 business days"

    $ celoRes = True

    jump installation

label robbery:

    scene office

    show cellores_talking

    voice "audio/voice/cellores_steal.ogg"
    cr "How do you plan to steal it over the phone?"

    hide cellores_talking
    show whoknoos_happy

    menu:
        "How do you plan to steal it over the phone?"

        "Umm... Uh...":
            voice "audio/voice/whoknoos_umm.ogg"
            w "Umm... Uh..."

            hide whoknoos_happy
            show cellores_talking

            voice "audio/voice/cellores_delivery.ogg"
            cr "Ok, sir if you'll just wait a few moments the polic- erm, delivery man will be there shortly to deliver your system."

            hide cellores_talking

            scene gameover_jail
            stop music
            play music "audio/canon-in-d.ogg" noloop
            call screen gameOverBadEnding
        "Easy, we're actually the same person so I can just take it myself":
            voice "audio/voice/whoknoos_sameperson.ogg"
            $ dreams += 1
            w "Easy, we're actually the same person so I can just take it myself"

            hide whoknoos_happy
            show cellores_talking

            voice "audio/voice/cellores_oh.ogg"
            cr "Oh"

            hide cellores_talking

            "Whoknoos then woke up and decided that he should probably get to calling CelloRes about getting a cellular respiration system for MitoConn"

            stop music
            play music "audio/voxel-revolution.ogg"

            jump phoneCall
        "I hid an Aperture Science Handheld Portal Device in your phone, I'm coming to get it!":
            voice "audio/voice/whoknoos_portal.ogg"
            w "I hid an Aperture Science Handheld Portal Device in your phone, I'm coming to get it!"

            hide whoknoos_happy
            $ stolen = True
            $ celoRes = True
            "Whoknoos then slithered through the phone, stole the cellular respiration system, and slithered back to MitoConn"
    jump installation

label installation:
    scene machine_room

    "Whoknoos then installed the cellular respiration system."
    "First he connected the input tubes to the oxygen, ADP, NADP+, and FADH and glucose tanks"
    "Then he installed the glycolosis module, the electron transport chain, and the citric acid cycler. These components perform oxidative phosphorylation, substrate level phosphorylation, and chemiosmosis to convert ADP to ATP, NADP+ to NADPH, and FADH to FADH₂."
    if fermentation:
        "Finally, he installed the fermentation module and connected the output tubes to transport the H₂O and CO₂ into the exhaust tunnel and the ATP to the machines that needed to be powered."
    else:
        "Finally, he connected the output tubes to transport the H₂O and CO₂ into the exhaust tunnel and the ATP to the machines that needed to be powered."
    jump apocalypse

label apocalypse:
    scene black
    stop music

    "{cps=5}3 years later...{/cps}{w=1}{nw}"

    stop music
    play music "audio/special-spotlight.ogg"

    scene evilhome
    show villain

    voice "audio/voice/evil_siphon.ogg"
    v "Ha ha ha, perish fools! I have invented the OxygenSiphon (patent pending)!"

    hide villain
    scene office
    show ceo_worried

    voice "audio/voice/ceo_apocalypse.ogg"
    c "What will we do? Will our company be able to survive the apocalypse?"

    hide ceo_worried


    if fermentation:
        show whoknoos_happy
        voice "audio/voice/whoknoos_fermentation.ogg"
        w "Don't worry boss, our cellular respiration system supports fermentation so it will be able to regenerate NAD+ while we don't have oxygen."
    else:
        show whoknoos_sad
        voice "audio/voice/whoknoos_nofermentation.ogg"
        w "Our cellular respiration system doesn't support fermentation so we will not be able to regernate NAD+ once the oxygen supply runs out. If only I had gotten the better model!"

    scene evilhome
    show villain

    voice "audio/voice/evil_oxygen.ogg"
    v "Say bye bye to your precious oxygen!"

    scene office_nooxygen

    if not fermentation:
        show whoknoos_sad

        voice "audio/voice/whoknoos_end.ogg"
        w "Well, looks like this is the end. Stay fresh cheese bags!"

        stop music
        play music "audio/space-fighter.ogg"

        scene evilhome
        call screen gameOverBadEnding
    else:
        show whoknoos_worried

        voice "audio/voice/whoknoos_hope.ogg"
        w "Let's hope this works."
        "As the air was removed, Whoknoos checked the meters for the system."

        hide whoknoos_worried
        show whoknoos_happy

        voice "audio/voice/whoknoos_decrease.ogg"
        w "Production has decreased but it's still going!"

        hide whoknoos_happy
        show ceo_hopeful

        voice "audio/voice/ceo_end.ogg"
        c "We've done it! Now all we need to do is save the world."

        hide ceo_hopeful
        show whoknoos_happy

        voice "audio/voice/whoknoos_photosynthesis.ogg"
        w "Don't worry boss! I've got just the solution, photosynthesis!"

        scene machine_room
        play music "audio/neon-laser-horizon.ogg"
        call screen gameOverBestEnding





screen gameOverBadEnding:
    vbox:
        xalign 0
        yalign 0
        text _("The End") size 42 bold True color "#cb4b16" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        text _("You didn't get the best ending! Click or press space to return to the main menu and try again to get a better ending.") size 20 color "#000000" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        if stolen and celoRes:
            text _("You got away with grand theft!") size 20 color "#859900" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        if fermentation and celoRes:
            text _("MitoConn survived the apocalypse!") size 20 color "#859900" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        key "dismiss" action Return()

screen gameOverBestEnding:
    vbox:
        xalign 0
        yalign 0
        text _("The End") size 42 bold True color "#cb4b16" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        text _("You got the best ending! Click or press space to return to the main menu.") size 20 color "#000000" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        if stolen and celoRes:
            text _("You got away with grand theft!") size 20 color "#859900" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        if fermentation and celoRes:
            text _("MitoConn survived the apocalypse!") size 20 color "#859900" outlines [ (absolute(2), "#ffffff", absolute(0), absolute(0)) ]
        key "dismiss" action Return()
